import userApi from 'api/userApi';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { login } from 'redux/actions/UserActions';
import { auth, facebookProvider, googleProvider } from 'services/firebase';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';

const Login = () => {
    const headerRef = useRef(null);
    const contentRef = useRef(null);
    const dispatch = useDispatch();

    useEffect(() => {
        const headerHeight = headerRef.current.getBoundingClientRect().height;
        contentRef.current.style.marginTop = `${headerHeight}px`;
    }, []);

    const [showPassword, setShowPassword] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleLogin = async (e) => {
        e.preventDefault();
        dispatch(login(
            email,
            password,
        ));
        try {
            const isPaid = await userApi.checkOrder(email);
            dispatch({
                type: 'SET_PAID',
                payload: true
            });
            localStorage.setItem('isPaid', true);
        } catch (err) {
            if (err.response.status === 403) {
                dispatch({
                    type: 'SET_PAID',
                    payload: false
                });
                localStorage.setItem('isPaid', false);
            }
        }
    };

    const handleSignInWithGoogle = () => {
        auth.signInWithPopup(googleProvider)
            .then(async (result) => {
                try {
                    const user = await userApi.findByEmail(result.user.email);
                    dispatch({
                        type: 'LOGIN_SUCCESS',
                        payload: user,
                    });
                    localStorage.setItem('user', JSON.stringify(user));
                } catch (err) {
                    if (err.response.status === 404) {
                        const userInserted = await userApi.insert({
                            fullName: result.user.displayName,
                            email: result.user.email,
                            image: result.user.photoURL,
                            mobile: result.user.phoneNumber ? result.user.phoneNumber : 'N/A',
                            sex: true,
                            dateOfBirth: '2000-01-01',
                            password: 'N/A',
                            position: 'USER',
                            status: true,
                        });
                        dispatch({
                            type: 'LOGIN_SUCCESS',
                            payload: userInserted,
                        });
                        localStorage.setItem('user', JSON.stringify(userInserted));
                    }
                } finally {
                    try {
                        const isPaid = await userApi.checkOrder(result.user.email);
                        dispatch({
                            type: 'SET_PAID',
                            payload: true
                        });
                        localStorage.setItem('isPaid', true);
                    } catch (err) {
                        if (err.response.status === 403) {
                            dispatch({
                                type: 'SET_PAID',
                                payload: false
                            });
                            localStorage.setItem('isPaid', false);
                        }
                    }
                }
            })
    }

    const handleSignInWithFacebook = () => {
        auth.signInWithPopup(facebookProvider)
            .then(async (result) => {
                try {
                    const user = await userApi.findByEmail(result.user.email);
                    dispatch({
                        type: 'LOGIN_SUCCESS',
                        payload: user,
                    });
                    localStorage.setItem('user', JSON.stringify(user));
                } catch (err) {
                    if (err.response.status === 404) {
                        const userInserted = await userApi.insert({
                            fullName: result.user.displayName,
                            email: result.user.email,
                            image: result.user.photoURL,
                            mobile: result.user.phoneNumber ? result.user.phoneNumber : 'N/A',
                            sex: true,
                            dateOfBirth: '2000-01-01',
                            password: 'N/A',
                            position: 'USER',
                            status: true,
                        });
                        dispatch({
                            type: 'LOGIN_SUCCESS',
                            payload: userInserted,
                        });
                        localStorage.setItem('user', JSON.stringify(userInserted));
                    }
                } finally {
                    try {
                        const isPaid = await userApi.checkOrder(result.user.email);
                        dispatch({
                            type: 'SET_PAID',
                            payload: true
                        });
                        localStorage.setItem('isPaid', true);
                    } catch (err) {
                        if (err.response.status === 403) {
                            dispatch({
                                type: 'SET_PAID',
                                payload: false
                            });
                            localStorage.setItem('isPaid', false);
                        }
                    }
                }
            })
    }

    return (
        <>
            <Header name="Login" ref={headerRef} />

            <main ref={contentRef}>
                <section
                    className="content-section"
                    style={{ background: 'url(https://themezinho.net/digiflex/images/section-pattern01.png)' }}
                >
                    <div className="container">
                        <div className="row align-items-center">
                            <div style={{ margin: 'auto' }}>
                                <div className="membership">
                                    <h6>Start Your Membership</h6>
                                    <form onSubmit={handleLogin}>
                                        <div className="form-group">
                                            <input
                                                value={email}
                                                onChange={e => setEmail(e.target.value)}
                                                type="text"
                                                placeholder="Your E-mail"
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input
                                                value={password}
                                                onChange={e => setPassword(e.target.value)}
                                                type={showPassword ? 'text' : 'password'}
                                                placeholder="Password"
                                            />
                                            <i
                                                onClick={() => setShowPassword(!showPassword)}
                                                className="lni lni-eye"
                                                style={{ cursor: 'pointer' }}
                                            ></i>
                                        </div>
                                        <div className="form-group">
                                            <input
                                                type="submit"
                                                value="LOGIN"
                                            />
                                        </div>
                                        <div className="register">
                                            Don't have account? &nbsp;
                                            <Link to='/signup'>Register here.</Link>
                                        </div>

                                        <div className="or">
                                            <span>or</span>
                                            <p>You can pick one of it to sign-in</p>
                                            <button
                                                type='button'
                                                onClick={handleSignInWithGoogle}
                                            >
                                                <i className="lni lni-google"></i>
                                                <small>with Google+</small>
                                            </button>
                                            <button
                                                type='button'
                                                onClick={handleSignInWithFacebook}
                                            >
                                                <i className="lni lni-facebook-filled"></i>
                                                <small>with Facebook</small>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            <Footer />
        </>
    )
}

export default Login;
