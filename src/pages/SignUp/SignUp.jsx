import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import React, { useEffect, useRef } from 'react';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import userApi from '../../api/userApi'
import { useHistory } from 'react-router-dom';
import Avatar from '@mui/material/Avatar';
import { color } from '@mui/system';
const SignUp = () => {
    const sliderRef = useRef(null);
    let history = useHistory();
    const contentRef = useRef(null);
    const [checkMail, setcheckMail] = React.useState(false);
    const [name, setName] = React.useState('');
    const handleNameChange = (event) => {
        setName(event.target.value);
    };
    const [email, setEmail] = React.useState('');
    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };
    const [phone, setPhone] = React.useState('');
    const handlePhoneChange = (event) => {
        setPhone(event.target.value);
    }
    const [date, setDate] = React.useState('');
    const handleDateChange = (event) => {
        setDate(event.target.value);
    }
    const [password, setPassword] = React.useState('');
    const handlePassChange = (event) => {
        setPassword(event.target.value);
    }
    const [sex, setSex] = React.useState(true);

    const handleSexChange = (event) => {
        setSex(event.target.value);
    };
    useEffect(() => {
        const sliderHeight = sliderRef.current.getBoundingClientRect().height;
        contentRef.current.style.marginTop = `${sliderHeight}px`;
    }, []);
    //
    const [avatar, setAvatar] = React.useState("")
    const handleAvaterChange = async (event) => {
        const formData = new FormData();
        formData.append("file", event.target.files[0]);
        formData.append("folder", "MovieProject/User");
        formData.append("upload_preset", "yeaeraza");
        const linkurl = await fetch('https://api.cloudinary.com/v1_1/cuongpham/image/upload', {
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(result => result.secure_url)
            .catch(() => "")
        setAvatar(linkurl);
    };
    const handleButtonClick = async () => {
        const param = {
            fullName: name,
            email: email,
            mobile: phone,
            sex: sex,
            dateOfBirth: date,
            password: password,
            position: "USER",
            image: avatar
        }
        const signup = await userApi.signup(param)
            .then(res => {
                setcheckMail(false)
                history.push("/login")
            })
            .catch(err => {
                setcheckMail(true)
            })
    };
    return (
        <>

            <Header name="Sign Up" ref={sliderRef} />
            <main ref={contentRef}>
                <section className="content-section" style={{ backgroundImage: "url('images/backgroudsignin.png')" }}>
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-7">
                                <div className="membership">
                                    <h6>Start Your Membership</h6>
                                    <div >
                                        <div style={{ color: "red", fontFamily: "'Roboto','Helvetica','Arial',sans-serif" }}>{checkMail && "Email đã tồn tại"}</div>
                                        <div className="form-group">
                                            <input type="text" placeholder="Full Name" onChange={handleNameChange} />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" placeholder="Your E-mail" onChange={handleEmailChange} />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" placeholder="Phone" onChange={handlePhoneChange} />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" placeholder="Date of birth" onChange={handleDateChange} />
                                        </div>
                                        <FormLabel id="demo-controlled-radio-buttons-group">Giới tính</FormLabel>
                                        <RadioGroup
                                            aria-labelledby="demo-controlled-radio-buttons-group"
                                            name="controlled-radio-buttons-group"
                                            value={sex}
                                            onChange={handleSexChange}

                                        >
                                            <div style={{ display: "flex" }}>

                                                <FormControlLabel value={true} control={<Radio />} label="Nam" />
                                                <FormControlLabel value={false} control={<Radio />} label="Nữ" />
                                            </div>
                                        </RadioGroup>
                                        <div className="form-group">
                                            <input type="password" placeholder="Password" onChange={handlePassChange} />
                                        </div>
                                        <div className="form-group">
                                            <FormLabel id="avatar">Thêm ảnh đại diện</FormLabel>
                                        </div>
                                        <div className="form-group">
                                            <input type="file" placeholder="avatar" id="avatar" name="avatar" onChange={handleAvaterChange} />
                                        </div>
                                        <Avatar style={{ width: "200px", height: "200px" }} src={avatar} />
                                        <div className="form-group">
                                            <input type="submit" value="START MEMBERSHIP" onClick={handleButtonClick} style={{ width: "100%" }} />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="col-lg-5">
                                <figure className="side-image">
                                    <img src="images/signup.jpg" alt="Image" />
                                </figure>
                            </div>

                        </div>

                    </div>

                </section>
            </main>
            <Footer />
        </>
    )
}
export default SignUp;