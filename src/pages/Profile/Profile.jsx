import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { IconButton } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import TextField from '@mui/material/TextField';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import userApi from '../../api/userApi';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';

const Profile = () => {
    const sliderRef = useRef(null);
    const contentRef = useRef(null);
    const { user } = useSelector(state => state.user);
    const date = new Date(user.dateOfBirth).toISOString().split('T')[0];
    const dispatch = useDispatch();
    const [edit, setEdit] = useState(true);
    const [showPassword, setShowPassword] = useState(false);
    const [param, setParam] = useState({
        id: user.id,
        status: user.status,
        fullName: user.fullName,
        email: user.email,
        dateOfBirth: date,
        mobile: user.mobile,
        position: user.position,
        sex: user.sex,
        image: user.image,
        password: user.password
    })
    const handleNameChange = (event) => {
        setParam({
            ...param,
            fullName: event.target.value
        });
    };
    const handleEmailChange = (event) => {
        setParam({
            ...param,
            email: event.target.value
        });
    };
    const handlePhoneChange = (event) => {
        setParam({
            ...param,
            mobile: event.target.value
        });
    }
    const handleDateChange = (event) => {
        setParam({
            ...param,
            dateOfBirth: event.target.value
        });
    }
    const handlePassChange = (event) => {
        setParam({
            ...param,
            password: event.target.value
        });
    }
    const handleSexChange = (event) => {
        setParam({
            ...param,
            sex: event.target.value
        });
    };
    const handleAvatarChange = async (event) => {
        const formData = new FormData();
        formData.append("file", event.target.files[0]);
        formData.append("folder", "MovieProject/User");
        formData.append("upload_preset", "yeaeraza");
        const linkurl = await fetch('https://api.cloudinary.com/v1_1/cuongpham/image/upload', {
            method: 'POST',
            body: formData,
        })
            .then(response => response.json())
            .then(result => result.secure_url)
            .catch(() => "")
        console.log(linkurl)
        setParam({
            ...param,
            image: linkurl
        });
    };
    const handleEditChange = (event) => {
        setEdit(false)
    };
    const handleUpdateChange = async () => {
        const update = await userApi.update(param)
        setEdit(true)
        localStorage.setItem("user", JSON.stringify(update))
        dispatch({
            type: "SET_USER",
            payload: update
        })
    }
    useEffect(() => {
        const sliderHeight = sliderRef.current.getBoundingClientRect().height;
        contentRef.current.style.marginTop = `${sliderHeight}px`;
    }, []);
    return (
        <>

            <Header name="Profile" ref={sliderRef} />
            <main ref={contentRef}>
                <section className="content-section" style={{ backgroundImage: "url('images/backgroudsignin.png')" }}>
                    <div className="container">
                        <div className="row align-items-center">
                            <div className='col-lg-7'>
                                <h4>Your Profile</h4>
                                <div style={{ marginTop: "20px" }}>
                                    <FormLabel id="demo-controlled-radio-buttons-group">Avatar</FormLabel>
                                    <Avatar style={{ width: "200px", height: "200px" }} src={param.image} />
                                    {
                                        !edit &&
                                        <input
                                            type="file"
                                            placeholder="avatar"
                                            id="avatar"
                                            name="avatar"
                                            onChange={handleAvatarChange}
                                            style={{ marginTop: '20px' }}
                                        />
                                    }
                                </div>
                                <div style={{ marginTop: "20px" }}>
                                    <TextField
                                        label="Full Name"
                                        defaultValue={param.fullName}
                                        InputProps={{
                                            readOnly: edit,
                                        }}
                                        onChange={handleNameChange}
                                    />
                                </div>
                                <div style={{ marginTop: "20px" }}>
                                    <TextField
                                        label="Email"
                                        defaultValue={param.email}
                                        InputProps={{
                                            readOnly: true,
                                        }}
                                        onChange={handleEmailChange}
                                    />
                                </div>
                                <div style={{ marginTop: "20px" }}>
                                    <TextField
                                        label="Phone"
                                        defaultValue={param.mobile}
                                        InputProps={{
                                            readOnly: edit,
                                        }}
                                        onChange={handlePhoneChange}
                                    />
                                </div>
                                <div style={{ marginTop: "20px" }}>
                                    <TextField
                                        label="Date of birth"
                                        defaultValue={param.dateOfBirth}
                                        InputProps={{
                                            readOnly: edit,
                                        }}
                                        onChange={handleDateChange}
                                    />
                                </div>
                                <div style={{ marginTop: "20px" }}>
                                    <TextField
                                        label="Position"
                                        defaultValue={param.position}
                                        InputProps={{
                                            readOnly: edit,
                                        }}
                                    />
                                </div>
                                <div style={{ marginTop: "20px" }}>

                                    <FormLabel id="demo-controlled-radio-buttons-group">Giới tính</FormLabel>
                                    <RadioGroup
                                        aria-labelledby="demo-controlled-radio-buttons-group"
                                        name="controlled-radio-buttons-group"
                                        value={param.sex}
                                        onChange={handleSexChange}

                                    >
                                        <div style={{ display: "flex" }}>

                                            <FormControlLabel value={true} control={<Radio />} label="Nam" disabled={edit} />
                                            <FormControlLabel value={false} control={<Radio />} label="Nữ" disabled={edit} />
                                        </div>
                                    </RadioGroup>
                                </div>
                                <div style={{ marginTop: "20px", position: 'relative' }}>
                                    <TextField
                                        label="Password"
                                        defaultValue={param.password}
                                        InputProps={{
                                            readOnly: edit,
                                        }}
                                        type={showPassword ? 'text' : 'password'}
                                        onChange={handlePassChange}
                                    >
                                    </TextField>
                                    {
                                        !edit &&
                                        <IconButton
                                            onClick={() => setShowPassword(!showPassword)}
                                            style={{ position: 'absolute', top: '22%', right: '34%', color: '#212529' }}
                                        >
                                            {!showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                                        </IconButton>
                                    }
                                </div>
                                <div style={{ marginTop: "20px" }}>
                                    <Button color="secondary" onClick={handleEditChange} disabled={!edit}>edit</Button>
                                    <Button variant="outlined" onClick={handleUpdateChange} disabled={edit}>Update</Button>
                                </div>
                            </div>
                            <div className="col-lg-5">
                                <figure className="side-image">
                                    <img src="images/signup.jpg" alt="Image" />
                                </figure>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
            <Footer />
        </>
    )
}
export default Profile;