import React, { useEffect, useRef, useState } from 'react';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';
import ContentSection from '../../components/ContentSection/ContentSection';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import { textAlign } from '@mui/system';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Button from '@mui/material/Button';
import moviesApi from '../../api/moviesApi';
const Movies = () => {
    const now = new Date().getFullYear();
    const sliderRef = useRef(null);
    const contentRef = useRef(null);
    const [param, setParam] = useState({
        type: "phim lẻ",
        limit: 112,
        page: 1
    })
    const [paramCount, setParamCount] = useState({
        type: "phim lẻ",
    })
    let render = []
    for (let i = now; i >= now - 20; i--) {

        render.push(<MenuItem key={i} value={i}>{i}</MenuItem>)
    }
    useEffect(() => {
        const sliderHeight = sliderRef.current.getBoundingClientRect().height;
        contentRef.current.style.marginTop = `${sliderHeight}px`;
    }, []);
    //phan trang vao day
    const [page, setPage] = React.useState(1);
    const handlePageChange = async (event, value) => {
        setPage(value);
        const paramPage = {
            ...param,
            page: value
        }
        const response = await moviesApi.getFilterAll(paramPage);
        setMovieList(response);
        setParam(paramPage)
    };
    //
    //
    const [count, setCount] = React.useState(0);
    // loc
    const [sort, setSort] = React.useState("");
    const handleSortChange = (event) => {
        setSort(event.target.value);
    };
    //
    const [location, setLocation] = React.useState("");
    const handleLocationChange = (event) => {
        setLocation(event.target.value);
    };
    //
    const [year, setYear] = React.useState("");
    const handleYearChange = (event) => {
        setYear(event.target.value);
    };
    //
    const [category, setCategory] = React.useState("");
    const handleCategoryChange = (event) => {
        setCategory(event.target.value);
    };
    //
    const [movieList, setMovieList] = useState([]);
    useEffect(async () => {
        const response = await moviesApi.getFilterAll(param);
        setMovieList(response);
        const responseCount = await moviesApi.getFilterCount(paramCount);
        setCount(responseCount);
    }, []);
    //phan trang
    let trang = Math.ceil(count / 12);
    //button
    const handleButton = async () => {
        const ParamButton = {
            type: "phim lẻ",
            year: year ? year : undefined,
            page: 1,
            sort: sort ? sort : undefined,
            category: category ? category : undefined,
            country: location ? location : undefined,
            limit: 12
        }
        const ParamButtonCount = {
            type: "phim lẻ",
            year: year ? year : undefined,
            category: category ? category : undefined,
            country: location ? location : undefined,
        }
        const response = await moviesApi.getFilterAll(ParamButton);
        const responseCount = await moviesApi.getFilterCount(ParamButtonCount);
        setCount(responseCount)
        setPage(1)
        setParamCount(ParamButtonCount)
        setParam(ParamButton)
        setMovieList(response);
    }
    return (
        <>

            <Header name="Movies" ref={sliderRef} />
            <main ref={contentRef}>
                <ContentSection
                    name="FIND ANYWHERE ELSE"
                    title="Movies For You"
                    textCenter={true}
                    light={false}
                    movies={movieList}
                    renderPagination={() => (
                        <Stack spacing={1} style={{ alignItems: 'center' }}>
                            <Pagination count={trang} page={page} onChange={handlePageChange} variant="outlined" shape="rounded" />
                        </Stack>
                    )}
                    renderSort={() => (
                        <FormControl sx={{ m: 1, minWidth: 150 }}>
                            <InputLabel id="demo-simple-select-autowidth-label">Sắp xếp</InputLabel>
                            <Select
                                labelId="demo-simple-select-autowidth-label"
                                id="demo-simple-select-autowidth"
                                value={sort}
                                onChange={handleSortChange}
                                autoWidth
                                label="Sắp xếp"
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={"post_time"}>Mới nhất</MenuItem>
                                <MenuItem value={"update_time"}>Mới cập nhật</MenuItem>
                                <MenuItem value={"count_view"}>Xem nhiều</MenuItem>
                            </Select>
                        </FormControl>
                    )}
                    renderLocation={() => (
                        <FormControl sx={{ m: 1, minWidth: 150 }}>
                            <InputLabel id="demo-simple-select-autowidth-label">Quốc gia</InputLabel>
                            <Select
                                labelId="demo-simple-select-autowidth-label"
                                id="demo-simple-select-autowidth"
                                value={location}
                                onChange={handleLocationChange}
                                autoWidth
                                label="Quốc gia"
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={"Mỹ"}>Mỹ</MenuItem>
                                <MenuItem value={"Trung Quốc"}>Trung Quốc</MenuItem>
                                <MenuItem value={"Nhật Bản"}>Nhật Bản</MenuItem>
                                <MenuItem value={"Hàn Quốc"}>Hàn Quốc</MenuItem>
                                <MenuItem value={"Anh"}>Anh</MenuItem>
                                <MenuItem value={"Đức"}>Đức</MenuItem>
                                <MenuItem value={"Pháp"}>Pháp</MenuItem>
                                <MenuItem value={"Nga"}>Nga</MenuItem>
                                <MenuItem value={"Thái Lan"}>Thái Lan</MenuItem>
                            </Select>
                        </FormControl>
                    )}
                    renderYear={() => (
                        <FormControl sx={{ m: 1, minWidth: 150 }}>
                            <InputLabel id="demo-simple-select-autowidth-label">Năm</InputLabel>
                            <Select
                                labelId="demo-simple-select-autowidth-label"
                                id="demo-simple-select-autowidth"
                                value={year}
                                onChange={handleYearChange}
                                autoWidth
                                label="Năm"
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                {
                                    render.map(param =>
                                        param)
                                }
                            </Select>
                        </FormControl>
                    )}
                    renderCategory={() => (
                        <FormControl sx={{ m: 1, minWidth: 150 }}>
                            <InputLabel id="demo-simple-select-autowidth-label">Thể loại</InputLabel>
                            <Select
                                labelId="demo-simple-select-autowidth-label"
                                id="demo-simple-select-autowidth"
                                value={category}
                                onChange={handleCategoryChange}
                                autoWidth
                                label="Thể loại"
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={"Phim Hành Động"}>Phim Hành Động</MenuItem>
                                <MenuItem value={"Phim Phiêu Lưu"}>Phim Phiêu Lưu</MenuItem>
                                <MenuItem value={"Phim Viễn Tưởng"}>Phim Viễn Tưởng</MenuItem>
                                <MenuItem value={"Phim Tình Cảm-Lãng Mạn"}>Phim Tình Cảm-Lãng Mạn</MenuItem>
                                <MenuItem value={"Phim Hồi Hộp-Gây Cấn"}>Phim Hồi Hộp-Gây Cấn</MenuItem>
                                <MenuItem value={"Phim Kinh Dị"}>Phim Kinh Dị</MenuItem>
                                <MenuItem value={"Phim Họat Hình"}>Phim Họat Hình</MenuItem>
                                <MenuItem value={"Phim Cổ Trang"}>Phim Cổ Trang</MenuItem>
                                <MenuItem value={"Phim Chiến Tranh"}>Phim Chiến Tranh</MenuItem>
                                <MenuItem value={"Phim Hài Hước"}>Phim Hài Hước</MenuItem>
                                <MenuItem value={"Phim Tâm Lý"}>Phim Tâm Lý</MenuItem>
                                <MenuItem value={"Phim Hình Sự"}>Phim Hình Sự</MenuItem>
                            </Select>
                        </FormControl>
                    )}
                    renderButton={() => (
                        <Button onClick={handleButton} variant="contained" sx={{ m: 1, minWidth: 100 }} style={{ lineHeight: '40px', fontWeight: "bold" }}>Lọc</Button>
                    )}
                />

            </main>
            <Footer />
        </>
    )
}

export default Movies;