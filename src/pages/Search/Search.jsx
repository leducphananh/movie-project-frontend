import { Pagination, Stack } from '@mui/material';
import React, { useEffect, useRef, useState } from 'react';
import moviesApi from '../../api/moviesApi';
import ContentSection from '../../components/ContentSection/ContentSection';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';

const limit = 12;

const Search = (props) => {

    const headerRef = useRef(null);
    const contentRef = useRef(null);
    useEffect(() => {
        const headerHeight = headerRef.current.getBoundingClientRect().height;
        contentRef.current.style.marginTop = `${headerHeight}px`;
    }, []);

    const [movieList, setMovieList] = useState([]);
    const [count, setCount] = useState(1);
    const [page, setPage] = useState(1);

    const search = new URLSearchParams(props.location.search).get('q');

    useEffect(() => {
        const fetch = async () => {
            let params = {
                key: search,
                limit,
                page
            };
            const movies = await moviesApi.searchMovies(params);
            setMovieList(movies);

            params = {
                key: search,
            }
            const count = await moviesApi.countSearch(params);
            setCount(Math.ceil(count / limit));
        };

        fetch();
    }, [page, search]);

    const handlePageChange = (event, page) => {
        setPage(page);
    }

    return (
        <>
            <Header name="Search" ref={headerRef} />

            <main ref={contentRef}>
                <ContentSection
                    name="FIND ANYWHERE ELSE"
                    title="Result For You"
                    textCenter={true}
                    light={false}
                    movies={movieList}
                    renderPagination={() => (
                        <Stack spacing={1} style={{ alignItems: 'center' }}>
                            <Pagination
                                count={count}
                                page={page}
                                onChange={handlePageChange}
                                variant="outlined"
                                shape="rounded"
                            />
                        </Stack>
                    )}
                />
            </main>

            <Footer />
        </>
    )
}

export default Search;