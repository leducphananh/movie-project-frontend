import Footer from 'components/Footer/Footer';
import React, { useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import moviesApi from '../../api/moviesApi';
import ContentSection from '../../components/ContentSection/ContentSection';
import Slider from '../../components/Slider/Slider';

const HomePage = () => {
    const history = useHistory();
    const sliderRef = useRef(null);
    const contentRef = useRef(null);
    const [movieList, setMovieList] = useState([]);
    const [tvShowList, setTvShowList] = useState([]);
    const [slider, setSlider] = useState([]);
    const [dealList, setDealList] = useState([]);

    useEffect(() => {
        const sliderHeight = sliderRef.current.getBoundingClientRect().height;
        contentRef.current.style.marginTop = `${sliderHeight}px`;
    }, []);

    useEffect(() => {
        const fetch = async () => {
            // fetch movies list
            let params = {
                sort: 'post_time',
                limit: 12,
                type: 'Phim lẻ'
            }
            const movieList = await moviesApi.getFilterAll(params);
            setMovieList(movieList);

            // fetch tvshows list
            params = {
                sort: 'post_time',
                limit: 12,
                type: 'Phim bộ'
            }
            const tvShowList = await moviesApi.getFilterAll(params);
            setTvShowList(tvShowList);

            // fetch slider 
            params = {
                sort: 'post_time',
                limit: 3,
            }
            const slider = await moviesApi.getFilterAll(params);
            setSlider(slider);

            // fetch deal
            const deal = await moviesApi.getAllDeal();
            setDealList(deal);
        }

        fetch();
    }, []);

    return (
        <>
            <Slider ref={sliderRef} slider={slider} />

            <main ref={contentRef}>
                <ContentSection
                    name="SUGGEST FOR YOU"
                    title="Movies For You"
                    textCenter={true}
                    light={false}
                    movies={movieList}
                />

                <ContentSection
                    name="FIND ANYWHERE ELSE"
                    title="TV Show For You"
                    textCenter={true}
                    light={true}
                    movies={tvShowList}
                />

                <section className="content-section">
                    <div className="video-bg">
                        <video src='https://themezinho.net/digiflex/videos/video01.mp4' autoPlay muted playsInline loop></video>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="section-title text-center light">
                                    <h6>START SECURE BROWSING</h6>
                                    <h2>Ready to Grab the deal?</h2>
                                </div>
                            </div>
                        </div>
                        {
                            dealList.length > 0 &&
                            <div className="row">
                                {
                                    dealList.map(deal => {
                                        return (
                                            <div className="col-lg-4" key={deal.id}>
                                                <div className="price-box">
                                                    <figure className="icon">
                                                        <img src="images/icon04.png" alt="Deal image" />
                                                    </figure>
                                                    <div className="months">{deal.month}-{deal.month > 1 ? 'months' : 'month'} plan</div>
                                                    <div className="price">
                                                        <b>{Number(deal.price * (100 - deal.discount) / 100 / deal.month).toLocaleString().replace(',', '.')}</b><span>₫</span>
                                                        <small>per month</small>
                                                    </div>
                                                    {
                                                        deal.discount > 0 && (
                                                            <>
                                                                <div className="save">Discount {deal.discount}%</div>
                                                                <div className="note">
                                                                    <u>{Number(deal.price).toLocaleString().replace(',', '.')}₫</u>
                                                                    <br />
                                                                    {Number(deal.price * (100 - deal.discount) / 100).toLocaleString().replace(',', '.')}₫ for {deal.month} {deal.month > 1 ? 'months' : 'month'}
                                                                </div>
                                                            </>
                                                        )
                                                    }
                                                    <button
                                                        onClick={() => {
                                                            history.push('/checkout');
                                                            localStorage.setItem('deal', JSON.stringify(deal));
                                                            window.scrollTo({ top: 0, behavior: 'smooth' });
                                                        }}
                                                    >
                                                        GET THE DEAL
                                                    </button>
                                                    <div className="guarantee">
                                                        <i className="lni lni-protection"></i>
                                                        30-days money-back guarantee
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        }
                    </div>
                </section>
            </main>

            <Footer />
        </>
    )
}

export default HomePage;
