import { PayPalButtons } from '@paypal/react-paypal-js';
import userApi from 'api/userApi';
import Footer from 'components/Footer/Footer';
import Header from 'components/Header/Header';
import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

const Checkout = () => {
    const history = useHistory();
    const headerRef = useRef(null);
    const contentRef = useRef(null);
    useEffect(() => {
        const headerHeight = headerRef.current.getBoundingClientRect().height;
        contentRef.current.style.marginTop = `${headerHeight}px`;
    }, []);

    const { user } = useSelector(state => state.user);
    const dispatch = useDispatch();
    const deal = JSON.parse(localStorage.getItem('deal'));

    return (
        <>
            <Header name="Checkout" ref={headerRef} />

            <main ref={contentRef} className='checkout'>
                <section
                    className="content-section"
                    style={{ background: 'url(https://themezinho.net/digiflex/images/section-pattern01.png)' }}
                >
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-7">
                                <div className="membership">
                                    <h6>User Information</h6>
                                    <div className="member-box">
                                        <figure>
                                            <img src={user.image} alt="Image" className="image" />
                                        </figure>
                                        <h5>{user.fullName}</h5>
                                        <small>{user.email}</small>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-5">
                                <div className="price-box">
                                    <figure className="icon">
                                        <img src="images/icon04.png" alt="Deal image" />
                                    </figure>
                                    <div className="months">{deal.month}-{deal.month > 1 ? 'months' : 'month'} plan</div>
                                    <div className="price">
                                        <b>{Number(deal.price * (100 - deal.discount) / 100 / deal.month).toLocaleString().replace(',', '.')}</b><span>₫</span>
                                        <small>per month</small>
                                    </div>
                                    {
                                        deal.discount > 0 && (
                                            <>
                                                <div className="save">Discount {deal.discount}%</div>
                                                <div className="note">
                                                    <u>{Number(deal.price).toLocaleString().replace(',', '.')}₫</u>
                                                    <br />
                                                    {Number(deal.price * (100 - deal.discount) / 100).toLocaleString().replace(',', '.')}₫ for {deal.month} {deal.month > 1 ? 'months' : 'month'}
                                                </div>
                                            </>
                                        )
                                    }
                                    <div className="guarantee">
                                        <i className="lni lni-protection"></i>
                                        30-days money-back guarantee
                                    </div>
                                </div>
                            </div>

                            <div style={{ margin: 'auto', marginTop: '20px' }}>
                                <PayPalButtons
                                    createOrder={(data, actions) => {
                                        return actions.order.create({
                                            purchase_units: [
                                                {
                                                    amount: {
                                                        value: `${(deal.price * (100 - deal.discount) / 100 / 23000).toFixed(2)}`,
                                                    },
                                                },
                                            ],
                                        });
                                    }}
                                    onApprove={(data, actions) => {
                                        return actions.order.capture()
                                            .then(async (details) => {
                                                const order = await userApi.checkout({
                                                    user: {
                                                        ...user
                                                    },
                                                    payment: {
                                                        type: 'digitalWallet',
                                                        totalMoney: deal.price * (100 - deal.discount) / 100,
                                                        name: 'Pay with Paypal'
                                                    },
                                                    deal: {
                                                        id: deal.id
                                                    }
                                                });
                                                localStorage.removeItem('deal');
                                                history.push('/');
                                                dispatch({
                                                    type: 'SET_PAID',
                                                    payload: true,
                                                });
                                                localStorage.setItem('isPaid', true);
                                            })
                                    }}
                                    onCancel={(data, actions) => {
                                        alert("Đơn hàng chưa được thanh toán");
                                    }}
                                    onError={(err) => {
                                        console.log("Thanh toán lỗi");
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            <Footer />
        </>
    )
}

export default Checkout;
