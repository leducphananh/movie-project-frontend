import React, { useEffect, useRef, useState } from 'react';
import ReactPlayer from 'react-player';
import { Link, useHistory, useParams } from 'react-router-dom';
import moviesApi from '../../api/moviesApi';
import Footer from '../../components/Footer/Footer';

const Watch = () => {

    const topRef = useRef(null);
    const contentRef = useRef(null);
    useEffect(() => {
        const topHeight = topRef.current.getBoundingClientRect().height;
        contentRef.current.style.marginTop = `${topHeight}px`;
    }, []);

    const { id } = useParams();
    const history = useHistory();
    const [movie, setMovie] = useState({});
    const [relatedMovies, setRelatedMovies] = useState([]);
    const [urlVideo, setUrlVideo] = useState(null);

    useEffect(() => {
        if (id) {
            const fetch = async () => {
                const movie = await moviesApi.getById(id);
                setMovie(movie);
                setUrlVideo(movie.movieLinks[0].url);

                const params = {
                    id,
                    category: movie.category,
                    limit: 6
                };
                const relatedMovies = await moviesApi.getRelatedMovies(params);
                setRelatedMovies(relatedMovies);
            }

            fetch();
        } else {
            history.push('/');
        }

        window.scrollTo({ top: 0, behavior: 'smooth' });
    }, [id, urlVideo]);

    const handleInsertCountView = () => {
        fetch('https://api.ipify.org/?format=json')
            .then((response) => response.json())
            .then(async (result) => {
                const params = {
                    ipAddress: result.ip,
                    movieId: id,
                };
                const insertCountView = await moviesApi.insertCountView(params);
            })
    }

    return (
        <>
            <main>
                <header
                    className="page-header single"
                    style={{ background: `url(${movie.image})` }}
                    ref={topRef}
                >
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="video-player">
                                    <span
                                        className="back-btn"
                                        style={{ cursor: 'pointer' }}
                                        onClick={() => { history.push('/') }}
                                    >
                                        <i className="lni lni-chevron-left"></i>
                                        BACK
                                    </span>
                                    <ReactPlayer
                                        url={urlVideo}
                                        playing={false}
                                        controls={true}
                                        width='984px'
                                        height='554px'
                                        playsinline
                                        onStart={handleInsertCountView}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </header>

                <section
                    className="content-section"
                    style={{ background: '#fff' }}
                    ref={contentRef}
                >
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="movie-info-box">
                                    <h2 className="name">{movie.name ? movie.name.slice(0, movie.name.lastIndexOf(' ')) : ''}<br />
                                        <strong>{movie.name ? movie.name.slice(movie.name.lastIndexOf(' ') + 1, movie.name.length) : ''}</strong>
                                    </h2>
                                    <ul className="features">
                                        <li>
                                            <div className="rate">
                                                <svg className="circle-chart" viewBox="0 0 30 30" width="40" height="40" fill="transparent" xmlns="http://www.w3.org/2000/svg">
                                                    <circle className="circle-chart__background" stroke="#eee" strokeWidth="2" fill="none" cx="15" cy="15" r="14"></circle>
                                                    <circle className="circle-chart__circle" stroke="#4eb04b" strokeWidth="2" strokeDasharray="50, 100" cx="15" cy="15" r="14"></circle>
                                                </svg>
                                                <b>{movie.imdb}</b> IMDB SCORE </div>
                                        </li>
                                        <li>
                                            <div className="year">{movie.year}</div>
                                        </li>
                                        <li>
                                            <div className="hd">{movie.quality} <b>HD</b></div>
                                        </li>
                                        <li>
                                            <div className="tags">{movie.category}</div>
                                        </li>
                                    </ul>
                                    <p className="description">
                                        {movie.content}
                                    </p>

                                    {
                                        movie.id &&
                                        (
                                            movie.movieLinks.length > 1 &&
                                            <div className="tvshows-episode">
                                                <ul>
                                                    {
                                                        movie.movieLinks.map(link => {
                                                            return (
                                                                <li
                                                                    key={link.id}
                                                                    onClick={(e) => {
                                                                        e.preventDefault();
                                                                        localStorage.setItem('movieId', id);
                                                                        history.push(`/watch-tvshows/${link.id}`)
                                                                    }}
                                                                >
                                                                    <span className="episode-link">
                                                                        Tập {link.episodeName}
                                                                    </span>
                                                                </li>
                                                            )
                                                        })
                                                    }
                                                </ul>
                                            </div>
                                        )
                                    }

                                    <div className="rate-box">
                                        <strong>Views: {movie.countView}</strong>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="movie-side-info-box">
                                    <figure><img src={movie.image} alt="Image" /></figure>
                                    <ul>
                                        <li><strong>Initial release: </strong> {movie.year}</li>
                                        <li><strong>Director:</strong> {movie.director}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="content-section" style={{ background: '#111' }}>
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="section-title light">
                                    <h6>FIND ANYWHERE ELSE</h6>
                                    <h2>Related Movies</h2>
                                </div>
                            </div>
                            {
                                relatedMovies.map(item => (
                                    <div key={item.id} className="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-6">
                                        <div className="video-thumb light">
                                            <figure className="video-image">
                                                <Link to={`/watch/${item.id}`}>
                                                    <img src={item.image} alt="Movie Image" />
                                                </Link>
                                                <div className="circle-rate">
                                                    <svg className="circle-chart" viewBox="0 0 30 30" width="100" height="100" xmlns="http://www.w3.org/2000/svg">
                                                        <circle className="circle-chart__background" stroke="#2f3439" strokeWidth="2" fill="none" cx="15" cy="15" r="14"></circle>
                                                        <circle className="circle-chart__circle" stroke="#4eb04b" strokeWidth="2" strokeDasharray="50,100" cx="15" cy="15" r="14"></circle>
                                                    </svg>
                                                    <b>{item.imdb}</b> </div>
                                                <div className="hd">{item.quality} <b>HD</b></div>
                                            </figure>
                                            <div className="video-content">
                                                <small className="range">{item.time} min</small>
                                                <h3 className="name">
                                                    <Link to={`/watch/${item.id}`}>{item.name}</Link>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </section>
                <Footer />
            </main>
        </>
    )
}

export default Watch;
