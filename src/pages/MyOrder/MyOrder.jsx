import userApi from 'api/userApi';
import Footer from 'components/Footer/Footer';
import Header from 'components/Header/Header';
import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';

const MyOrder = () => {

    const headerRef = useRef(null);
    const contentRef = useRef(null);
    useEffect(() => {
        const headerHeight = headerRef.current.getBoundingClientRect().height;
        contentRef.current.style.marginTop = `${headerHeight}px`;
    }, []);

    const { user } = useSelector(state => state.user);
    const [listOrder, setListOrder] = useState([]);

    useEffect(() => {
        const fetch = async () => {
            const orders = await userApi.findOrderByEmail(user.email);
            setListOrder(orders);
        }

        fetch();
    }, []);

    return (
        <>
            <Header name="My Order" ref={headerRef} />

            <main ref={contentRef}>
                <section
                    className="content-section"
                    style={{ background: 'url(https://themezinho.net/digiflex/images/section-pattern01.png)' }}
                >
                    <div className="container">
                        <div className="row align-items-center">
                            <div style={{ margin: 'auto' }}>
                                <div className="list-order">
                                    <h6>View Order</h6>
                                    {
                                        listOrder.length > 0
                                            ?
                                            <div style={{ display: 'flex', width: '100%', flexWrap: 'wrap', justifyContent: 'space-around' }}>
                                                {
                                                    listOrder.map(order => {
                                                        const { deal, payment } = order;
                                                        return (
                                                            <div className="price-box" key={order.id}>
                                                                <figure className="icon">
                                                                    <img src="images/icon04.png" alt="Deal image" />
                                                                </figure>
                                                                <div className="months">{deal.month}-{deal.month > 1 ? 'months' : 'month'} plan</div>
                                                                <div className="price">
                                                                    <b>{Number(deal.price * (100 - deal.discount) / 100 / deal.month).toLocaleString().replace(',', '.')}</b><span>₫</span>
                                                                    <small>per month</small>
                                                                </div>
                                                                {
                                                                    deal.discount > 0 && (
                                                                        <>
                                                                            <div className="save">Discount {deal.discount}%</div>
                                                                            <div className="note">
                                                                                <u>{Number(deal.price).toLocaleString().replace(',', '.')}₫</u>
                                                                                <br />
                                                                                {Number(deal.price * (100 - deal.discount) / 100).toLocaleString().replace(',', '.')}₫ for {deal.month} {deal.month > 1 ? 'months' : 'month'}
                                                                            </div>
                                                                        </>
                                                                    )
                                                                }
                                                                <div className="guarantee">
                                                                    <i className="lni lni-calendar"></i> &nbsp;
                                                                    From {order.startAt.substring(0, order.startAt.indexOf('T'))} - To {order.endAt.substring(0, order.endAt.indexOf('T'))}
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </div>
                                            :
                                            <div>
                                                <img
                                                    src='https://image.shutterstock.com/image-illustration/red-stamp-order-now-260nw-216295078.jpg'
                                                    alt='No order'
                                                    style={{ marginBottom: '20px' }}
                                                />
                                                Nothing to display.
                                            </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>

            <Footer />
        </>
    )
}

export default MyOrder;
