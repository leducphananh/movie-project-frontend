import axiosClient from './axiosClient';

const moviesApi = {
    getAll: (params) => {
        const url = '/api/movie/find-all';
        return axiosClient.get(url, { params });
    },

    getFilterAll: (params) => {
        const url = '/api/movie/filter';
        return axiosClient.get(url, { params });
    },

    getFilterCount: (params) => {
        const url = "/api/movie/count-filter"
        return axiosClient.get(url, { params });
    },

    getById: (id) => {
        const url = `/api/movie/find/${id}`;
        return axiosClient.get(url);
    },

    getRelatedMovies: (params) => {
        const url = '/api/movie/same-movie';
        return axiosClient.get(url, { params });
    },

    searchMovies: (params) => {
        const url = '/api/movie/search';
        return axiosClient.get(url, { params });
    },

    countSearch: (params) => {
        const url = '/api/movie/count-search';
        return axiosClient.get(url, { params });
    },

    getAllDeal: () => {
        const url = '/api/deal/find-all';
        return axiosClient.get(url);
    },

    insertCountView: (body) => {
        const url = 'api/count-view/insert';
        return axiosClient.post(url, body);
    }
}

export default moviesApi;