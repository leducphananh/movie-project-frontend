import axiosClient from "./axiosClient";

const userApi = {
    login: (params) => {
        const url = '/api/user/login';
        return axiosClient.get(url, { params });
    },

    signup: (body) => {
        const url = '/api/user/insert';
        return axiosClient.post(url, body);
    },

    checkout: (order) => {
        const url = '/api/order/insert';
        return axiosClient.post(url, order);
    },

    findByEmail: (email) => {
        const url = '/api/user/find-by-email';
        return axiosClient.get(`${url}/${email}`);
    },

    insert: (user) => {
        const url = '/api/user/insert';
        return axiosClient.post(url, user);
    },

    checkOrder: (email) => {
        const url = '/api/order/check-order-expire';
        return axiosClient.get(`${url}/${email}`);
    },

    update: (body) => {
        const url = '/api/user/update';
        return axiosClient.put(url, body);
    },

    findOrderByEmail: (email) => {
        const url = '/api/order/find-by-email';
        return axiosClient.get(`${url}/${email}`);
    },
}

export default userApi;
