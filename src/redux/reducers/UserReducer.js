const initialState = {
    user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null,
    isPaid: localStorage.getItem('isPaid') ? JSON.parse(localStorage.getItem('isPaid')) : false,
    error: null,
}

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                user: action.payload,
            }
        case 'LOGOUT':
            return {
                ...state,
                user: null,
            }
        case 'SET_PAID':
            return {
                ...state,
                isPaid: action.payload,
            }
        case 'SET_USER':
            return {
                ...state,
                user: action.payload
            }
        default:
            return state;
    }
}

export default UserReducer;
