import userApi from 'api/userApi';

export const login = (emailInput, passwordInput) => async dispatch => {
    try {
        const user = await userApi.login({
            email: emailInput,
            password: passwordInput
        });
        dispatch({
            type: 'LOGIN_SUCCESS',
            payload: user,
        });
        const { password, ...rest } = user;
        localStorage.setItem('user', JSON.stringify({
            ...rest
        }));
    } catch (error) {
        console.log(error);
    }
}

export const logout = () => async dispatch => {
    try {
        dispatch({
            type: 'LOGOUT',
        });
        localStorage.removeItem('user');
        localStorage.removeItem('isPaid');
    } catch (error) {
        console.log(error);
    }
}
