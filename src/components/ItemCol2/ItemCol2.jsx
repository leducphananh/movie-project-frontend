import React from 'react';
import { useHistory } from 'react-router-dom';

const ItemCol2 = (props) => {

    const history = useHistory();
    const { id, image, name, time, quality, light, imdb } = props;

    return (
        <div
            className="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-6"
            onClick={() => {
                history.push(`/watch/${id}`);
                window.scrollTo({ top: 0, behavior: 'smooth' });
            }}
            style={{ cursor: 'pointer' }}
        >
            <div className={`video-thumb ${light && 'light'}`}>
                <figure className="video-image">
                    <a href="#">
                        <img src={image} alt="Video image" />
                    </a>
                    <div className="circle-rate">
                        <svg className="circle-chart" viewBox="0 0 30 30" width="100" height="100"
                            xmlns="http://www.w3.org/2000/svg">
                            <circle className="circle-chart__background" stroke="#2f3439" strokeWidth="2" fill="none" cx="15"
                                cy="15" r="14"></circle>
                            <circle className="circle-chart__circle" stroke="#4eb04b" strokeWidth="2" strokeDasharray="50,100"
                                cx="15" cy="15" r="14"></circle>
                        </svg>
                        <b>{imdb}</b>
                    </div>
                    <div className="hd">{quality} <b>HD</b></div>
                </figure>
                <div className="video-content">
                    <small className="range">{time} min</small>
                    <h3 className="name">
                        <a>{name}</a>
                    </h3>
                </div>
            </div>
        </div >
    )
}

export default ItemCol2;
