import React from 'react';

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="row">
          <div className="col-lg-4">
            <h5 className="call-us">Questions? <strong>Call 0850-380-6444</strong></h5>
            <p> Format for custom post types that are not book
              or you can use else if to <u>specify a second</u> post type the
              same way as above. </p>
            <div className="language"> <i className="lni lni-world"></i>
              <select>
                <option>English</option>
                <option>Spanish</option>
              </select>
            </div>
          </div>

          <div className="col-lg-2 offset-lg-1 col-md-4">
            <h6 className="widget-title">DIGIFLEX</h6>
            <ul className="footer-menu">
              <li><a href="digiflex.html">Digiflex</a></li>
              <li><a href="devices.html">Devices</a></li>
              <li><a href="tips.html">Tips</a></li>
              <li><a href="contact.html">Contact</a></li>
            </ul>
          </div>

          <div className="col-lg-2 col-md-4">
            <h6 className="widget-title">SUPPORT</h6>
            <ul className="footer-menu">
              <li><a href="faq.html">FAQ</a></li>
              <li><a href="help-center.html">Help Center</a></li>
              <li><a href="account.html">Account</a></li>
              <li><a href="support.html">Support <i className="lni lni-question-circle"></i></a></li>
              <li><a href="media-center.html">Media Center</a></li>
            </ul>
          </div>

          <div className="col-lg-2 col-md-4">
            <h6 className="widget-title">POLICIES</h6>
            <ul className="footer-menu">
              <li><a href="privacy-policy.html">Privacy Policy</a></li>
              <li><a href="terms-agreement.html">Terms & Agreement</a></li>
              <li><a href="legal-notices.html">Legal Notices</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div className="bottom-bar">
        <div className="container"> <span>© 2020 Digiflex | Online Movie Streaming</span> <span>Site create by <a href="#">Themezinho</a></span> </div>
      </div>
    </footer>
  )
}

export default Footer;
