import React, { forwardRef } from 'react';

const Header = (props, ref) => {
    const { name } = props;
    return (
        <header className="page-header" ref={ref}>
            <div className="container">
                <h1>{name}</h1>
            </div>
        </header>
    )
}
export default forwardRef(Header);