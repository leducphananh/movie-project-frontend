import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, NavLink, useHistory } from 'react-router-dom';

const Navbar = ({ navItems }) => {

    const searchRef = useRef(null);
    const history = useHistory();

    const [showSearch, setShowSearch] = useState(false);
    const [search, setSearch] = useState('');
    const { user } = useSelector(state => state.user);

    const handleSearchClick = () => {
        document.body.classList.toggle('overflow');
        setShowSearch(!showSearch);
    }

    useEffect(() => {
        if (!showSearch) {
            return;
        }
        const clickOutSideHandler = (e) => {
            if (e.target === searchRef.current) {
                return;
            }
            setShowSearch(false);
            document.body.classList.remove('overflow');
        }
        window.document.addEventListener('click', clickOutSideHandler);

        return () => {
            window.document.removeEventListener('click', clickOutSideHandler);
        }
    }, [showSearch]);

    const handleSubmitForm = (e) => {
        e.preventDefault();
        history.push(`/search?q=${search}`);
        setShowSearch(false);
        document.body.classList.remove('overflow');
        setSearch('');
    }

    return (
        <>
            <nav className="mobile-menu">
                <div className="inner">
                    <div className="mobile-search">
                        <h6>Type movie or tv show name to find it</h6>
                        <form>
                            <input type="search" placeholder="Search here" />
                            <input type="submit" value="FIND" />
                        </form>
                    </div>
                    {
                        user ?
                            <Link to='/logout' className="button-account">
                                <i className="lni lni-user"></i> LOG OUT
                            </Link>
                            :
                            <Link to='/login' className="button-account">
                                <i className="lni lni-user"></i> LOG IN
                            </Link>
                    }
                    <div className="site-menu">
                        <ul>
                            {navItems.map((navItem, index) =>
                                <li key={index}>
                                    {
                                        navItem.label &&
                                        <NavLink
                                            to={navItem.path}
                                        >
                                            {navItem.label}
                                        </NavLink>
                                    }
                                </li>
                            )}
                        </ul>
                    </div>
                </div>
            </nav>
            <nav className="navbar">
                <div className="logo">
                    <Link to='/'>
                        <img src="https://themezinho.net/digiflex/images/logo.png" alt="Logo" />
                    </Link>
                </div>
                <div className="site-menu">
                    <ul>
                        {navItems.map((navItem, index) =>
                            <li key={index}>
                                {
                                    navItem.label &&
                                    <NavLink
                                        to={navItem.path}
                                    >
                                        {navItem.label}
                                    </NavLink>
                                }
                            </li>
                        )}
                    </ul>
                </div>
                <div className="user-menu">
                    <div className="navbar-search">
                        <i
                            className="lni lni-search-alt"
                            onClick={handleSearchClick}
                        >
                        </i>
                    </div>
                    <div className="navbar-notify">
                        <i
                            className="material-symbols-outlined"
                            onClick={() => history.push('/my-order')}
                        >
                            shopping_bag
                        </i>
                    </div>
                    <div className="navbar-account">
                        {
                            user ?
                                <>
                                    <Link to='/logout' className="button-account">
                                        LOG OUT
                                    </Link>
                                    <img
                                        src={user.image}
                                        alt='Avatar'
                                        style={{ cursor: 'pointer' }}
                                        onClick={() => {
                                            history.push('/profile');
                                        }}
                                    />
                                </>
                                :
                                <Link to='login' className="button-account">
                                    <i className="lni lni-user"></i> LOG IN
                                </Link>
                        }
                    </div>
                </div>
                <div className="hamburger-menu">
                    <button className="hamburger">
                        <svg width="45" height="45" viewBox="0 0 100 100">
                            <path className="line line1"
                                d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
                            <path className="line line2" d="M 20,50 H 80" />
                            <path className="line line3"
                                d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
                        </svg>
                    </button>
                </div>
            </nav>

            <section className={`search-box ${showSearch ? 'active' : ''}`} >
                <div className="container">
                    <h6>Type movie or tv show name to find it</h6>
                    <form onSubmit={handleSubmitForm}>
                        <input
                            ref={searchRef}
                            type="search"
                            placeholder="Search here"
                            value={search}
                            onChange={(e) => setSearch(e.target.value)}
                        />
                        <input type="submit" value="FIND" />
                    </form>
                </div>
            </section>
        </>
    )
}

export default Navbar;
