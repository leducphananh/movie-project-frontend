import React from 'react';
import ItemCol2 from '../ItemCol2/ItemCol2';
import Poster from '../Poster/Poster';

const ContentSection = (props) => {

    const { name, title, movies, light, textCenter,
        renderSort, renderShowMore, renderPagination, renderLocation, renderYear, renderCategory, renderButton } = props;
    return (
        <section className="content-section" style={{ background: `${light ? '#111111' : ''}` }}>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div
                            className={`section-title ${textCenter && 'text-center'} ${light && 'light'}`}
                        >
                            <h6>{name}</h6>
                            <h2>{title}</h2>
                        </div>
                        <div style={{ textAlign: 'right' }}>
                            <span style={{ textAlign: 'left' }} >
                                {renderSort && renderSort()}
                                {renderLocation && renderLocation()}
                                {renderYear && renderYear()}
                                {renderCategory && renderCategory()}
                                {renderButton && renderButton()}
                            </span>
                        </div>
                    </div>

                    {
                        movies.map(movie => (
                            <ItemCol2
                                key={movie.id}
                                {...movie}
                                light={light}
                            />
                        ))
                    }

                </div>
                {renderPagination && renderPagination()}
                {renderShowMore && renderShowMore()}
            </div>
        </section>
    )
}

export default ContentSection;