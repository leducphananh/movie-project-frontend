import React from 'react';

const ItemHome = ({ image, category, name, year, type }) => {

    return (
        <div className="col-lg-3 col-sm-6">
            <div className="category-thumb">
                <figure className="category-image">
                    <img src={image} alt="Category Image" />
                </figure>
                <div className="category-content">
                    <ul className="tags">
                        {
                            category.split(', ').map((category, index) => (
                                <li key={index}>{category}</li>
                            ))
                        }
                    </ul>
                    <h3 className="name">{name}</h3>
                    <div className="play-btn"><a href="movie-single.html">+</a></div>
                    <small className="details">{type} <span>-</span> {year}</small>
                </div>
            </div>
        </div>
    );
}

export default ItemHome;
