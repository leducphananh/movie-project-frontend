import React, { forwardRef } from 'react';
import SwiperCore, { Autoplay, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import { useHistory } from 'react-router-dom';

const Slider = (props, ref) => {

    // SwiperCore.use([Autoplay]);

    const history = useHistory();
    const { slider } = props;

    return (
        <header className="slider" ref={ref}>
            <Swiper
                className="main-slider"
                modules={[Pagination, Autoplay]}
                autoplay={{
                    delay: 5000,
                    disableOnInteraction: false,
                    pauseOnMouseEnter: true,
                }}
                loop={true}
                grabCursor={true}
                spaceBetween={0}
                slidesPerView={1}
                pagination={{
                    clickable: true,
                    renderBullet: (index, className) => {
                        return '<span class="' + className + '"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><circle r="13" cy="15" cx="15"></circle></svg></span>';
                    },
                }}
            >
                {slider.map(item => {
                    const { name, imdb, year, quality, category, image, content } = item;
                    const firstName = name.slice(0, name.lastIndexOf(' '));
                    const lastName = name.slice(name.lastIndexOf(' ') + 1, name.length);
                    let _content = content;
                    if (_content.length > 200) {
                        _content = content.substring(0, 200) + '...';
                    }
                    const desc1 = _content.slice(0, _content.indexOf(_content.split(' ')[20]));
                    const desc2 = _content.slice(_content.indexOf(_content.split(' ')[20]), _content.length)
                    return (
                        <SwiperSlide key={item.id}>
                            <div className="slide-inner bg-image" style={{ background: `url(${image})` }}>
                                <div className="container">
                                    <h6 className="tagline">NEW RELEASES</h6>
                                    <h2 className="name">{firstName}
                                        <br />
                                        <strong>{lastName}</strong>
                                    </h2>
                                    <ul className="features">
                                        <li>
                                            <div className="rate">
                                                <svg className="circle-chart" viewBox="0 0 30 30" width="40" height="40"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <circle className="circle-chart__background" stroke="#2f3439" strokeWidth="2" fill="none" cx="15"
                                                        cy="15" r="14"></circle>
                                                    <circle className="circle-chart__circle" stroke="#4eb04b" strokeWidth="2" strokeDasharray="50,100"
                                                        cx="15" cy="15" r="14"></circle>
                                                </svg>
                                                <b>{imdb}</b> IMDB SCORE
                                            </div>
                                        </li>
                                        <li>
                                            <div className="year">{year}</div>
                                        </li>
                                        <li>
                                            <div className="hd">{quality} <b>HD</b></div>
                                        </li>
                                        <li>
                                            <div className="tags">{category}</div>
                                        </li>
                                    </ul>
                                    <p className="description">
                                        {desc1}
                                        <br />
                                        {desc2}
                                    </p>
                                    <a onClick={(e) => {
                                        e.preventDefault();
                                        history.push(`/watch/${item.id}`);
                                    }}
                                        className="play-btn"
                                        style={{ cursor: 'pointer' }}
                                    >
                                        WATCH MOVIE
                                    </a>
                                </div>
                            </div>
                        </SwiperSlide>
                    )
                })}
            </Swiper>
        </header >
    )
}

export default forwardRef(Slider);
