import Checkout from 'pages/Checkout/Checkout';
import HomePage from 'pages/HomePage/HomePage';
import Login from 'pages/Login/Login';
import Movies from 'pages/Movies/Movies';
import Profile from 'pages/Profile/Profile';
import MyOrder from 'pages/MyOrder/MyOrder';
import Search from 'pages/Search/Search';
import SignUp from 'pages/SignUp/SignUp';
import TVShow from 'pages/TVShow/TVShow';
import Watch from 'pages/Watch/Watch';
import { Suspense } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { logout } from 'redux/actions/UserActions';
import { auth } from 'services/firebase';
import Navbar from './components/Navbar/Navbar';
import routes from './routes/routes';
import WatchTvShows from 'pages/Watch/WatchTvShows';

function App() {

  const { user, isPaid } = useSelector(state => state.user);
  const dispatch = useDispatch();

  return (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Navbar navItems={routes} />

        <Switch>
          <Route
            exact
            path='/'
            component={HomePage}
          />

          <Route
            path='/movies'
            component={Movies}
          />

          <Route
            path='/tvshow'
            component={TVShow}
          />

          <Route
            path='/search'
            component={Search}
          />

          <Route
            path='/watch/:id'
            render={(prop) => {
              if (user && isPaid) {
                return (
                  <Watch {...prop} />
                )
              }
              if (user && !isPaid) {
                return (
                  <HomePage {...prop} />
                )
              }
              return <Redirect to='/login' />
            }}
          />

          <Route
            path='/watch-tvshows/:id'
            render={(prop) => {
              if (user && isPaid) {
                return (
                  <WatchTvShows {...prop} />
                )
              }
              if (user && !isPaid) {
                return (
                  <HomePage {...prop} />
                )
              }
              return <Redirect to='/login' />
            }}
          />

          <Route
            path='/login'
            render={(prop) => {
              if (!user) {
                return (
                  <Login {...prop} />
                )
              }
              return <Redirect to='/' />
            }}
          />

          <Route
            path='/logout'
            render={() => {
              if (user) {
                auth.signOut()
                  .then(() => {
                    dispatch(logout());
                    localStorage.removeItem('user');
                    localStorage.removeItem('isPaid');
                  })
                  .catch(err => {
                    console.log(err.message);
                  })
              }
              return <Redirect to='/login' />
            }}
          />

          <Route
            path='/signup'
            render={(prop) => {
              if (!user) {
                return (
                  <SignUp {...prop} />
                )
              }
              return <Redirect to='/' />
            }}
          />

          <Route
            path='/checkout'
            render={(prop) => {
              if (user) {
                return (
                  <Checkout {...prop} />
                )
              }
              return <Redirect to='/login' />
            }}
          />

          <Route
            path='/profile'
            render={(prop) => {
              if (user) {
                return (
                  <Profile {...prop} />
                )
              }
              return <Redirect to='/login' />
            }}
          />

          <Route
            path='/my-order'
            render={(prop) => {
              if (user) {
                return (
                  <MyOrder {...prop} />
                )
              }
              return <Redirect to='/login' />
            }}
          />
        </Switch>
      </Suspense>
    </Router >
  );
}

export default App;
