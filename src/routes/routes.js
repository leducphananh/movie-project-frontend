import Profile from 'pages/Profile/Profile';
import HomePage from '../pages/HomePage/HomePage';
import Login from '../pages/Login/Login';
import Movies from '../pages/Movies/Movies';
import Search from '../pages/Search/Search';
import SignUp from '../pages/SignUp/SignUp';
import TVShow from '../pages/TVShow/TVShow';
import Watch from '../pages/Watch/Watch';

const routes = [
    {
        path: '/',
        label: 'HOME',
        component: HomePage
    },
    {
        path: '/movies',
        label: 'MOVIES',
        component: Movies
    },
    {
        path: '/tvshow',
        label: 'TVSHOW',
        component: TVShow
    },
    {
        path: '/watch/:id',
        component: Watch
    },
    {
        path: '/search',
        component: Search
    },
    {
        path: '/signup',
        component: SignUp
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '/profile',
        component: Profile
    },
]

export default routes;